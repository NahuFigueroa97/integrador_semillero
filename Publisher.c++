#include <WiFi.h>
#include <PubSubClient.h>
#include <Arduino.h>
#include <NTPClient.h>

#define sizeBuffer 70
#define sensibilidadmV 10.00
#define TensionmV 3270.00
#define Resolucion 4096.00
#define sampleTime 1000
#define port 1883
#define  LED  2
#define ToSecond(x) x/1000
#define Temperature 32
#define Factor1 TensionmV/Resolucion
#define Factor Factor1/sensibilidadmV
#define maxSample 15
#define GTMconfig -10750
#define Message "Id = %d; temp = %.2f; timestamp = %s"

String Hour;

WiFiUDP ntpUDP;
NTPClient timeClient(ntpUDP);

typedef struct
{
  int ID;
  float temperatura, lastMsg, now;
}data_t;

data_t data;

const char* ssid = "Figueroa39_2.4GHz";
const char* password = "Nahuvsnacho97";
const char* mqtt_server = "test.mosquitto.org";
const char* Topic = "Invap/Temperatura";

void setup_wifi();
void callback(char* topic, byte* payload, int length);

WiFiClient espClient;
PubSubClient client(espClient);
char msg[sizeBuffer];

void setup() {
  data.ID = 0;
  pinMode(LED, OUTPUT);     
  Serial.begin(115200);
  setup_wifi();
  client.setServer(mqtt_server, 1883);
  client.setCallback(callback);
  timeClient.begin();
  timeClient.setTimeOffset(GTMconfig);
}

void setup_wifi() {
  delay(10);
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}

void callback(char* topic, byte* payload, int length) {
  Serial.println();
  if ((char)payload[0] == '1') {
    digitalWrite(LED, LOW);  
  } else {
    digitalWrite(LED, HIGH);
  }
}

void reconnect() {
  while (!client.connected()) {
    if (client.connect("ESP32Client")) {}
     else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      delay(5000);
    }
  }
}

void loop() {
  timeClient.update();
  if (!client.connected()) {
    reconnect();
  }
  client.loop();
  data.now = millis();
  if (data.now - data.lastMsg > sampleTime) {
    data.temperatura = analogRead(Temperature)*Factor ;
    data.lastMsg = data.now;
    Hour = timeClient.getFormattedTime();
    sprintf (msg, Message, data.ID, data.temperatura, Hour);
    Serial.println(msg);
    client.publish(Topic, msg);
  }
  }
