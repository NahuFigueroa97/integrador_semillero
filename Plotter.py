import paho.mqtt.client as mqtt
from const import *
from numpy import *
from math import *
import matplotlib.pyplot as plt
import sys
import csv
import pandas as pd
import pathlib
import os
from dateutil import parser

semOne = False
samples = [0, 0, 0, 0]
sem = [0, 0, 0, 0]
receiverDecode = []
receiverDecodeData = [[],[],[],[]]

file = open("data.csv", "a", newline = "")
path = str(pathlib.Path().absolute()) + "/data.csv"
writer = csv.writer(file)

def on_connect(client, userdata, flags, rc):
	print('connected (%s)' % client._client_id)
	client.subscribe(Topic)

def on_message(client, userdata, message):
	global semOne
	receiver = str(message.payload.decode("utf-8") )
	receiverJson = receiver.replace(" ", "").split(';')
	if (samples[IndexNahuel] and samples[IndexDiego] and samples[IndexMayra] and samples[IndexTomas]) < maxSample:
		if semOne == False:
			for i in range(FieldZero, FieldThree, 1):
				receiverDecode.append(receiverJson[i].split('=')[IndexTittle])
			writer.writerow(receiverDecode)
			receiverDecode.clear()
			semOne = True
		for i in range(FieldZero, FieldThree, 1):
			for j in range (IndexNahuel, IndexTomas, 1):
				if int(receiverJson[IndexId].split('=')[IndexValue]) == j and sem[j]== False:
					receiverDecodeData[j].append(receiverJson[i].split('=')[IndexValue])
					if samples[j] < maxSample:
						sem[j] = False
					else :
						sem[j] = True
						samples[i] += 1
		for i in range(IndexNahuel, IndexTomas, 1):
			if int(receiverJson[IndexId].split('=')[IndexValue]) == i and sem[i] == False:
				print(receiverDecodeData[i])
				writer.writerow(receiverDecodeData[i])
				samples[i] += 1
				receiverDecodeData[i].clear()
	else:
		file.close()
		datos = pd.read_csv("data.csv")
		plt.figure()
		names = {
			IndexNahuel: "Nahuel",
			IndexDiego: "Diego",
			IndexMayra: "Mayra",
			IndexTomas: "Tomas",
		}
		for idx in [IndexNahuel, IndexDiego, IndexMayra, IndexTomas]:
			data = datos.query(f"Id=={idx}")
			plt.plot(
				data["timestamp"].apply(parser.parse),
				data["temp"].astype(float),
				label=names[idx],
			)
		plt.legend()
		plt.grid()
		plt.xlabel("Tiempo")
		plt.ylabel("T [°C]")
		plt.savefig("Measurements.png")
		plt.show()
		os.remove(path)


def main():
	client = mqtt.Client()
	client.on_connect = on_connect
	client.on_message = on_message
	client.connect(Host, port)
	client.loop_forever()

if __name__ == '__main__':
	main()
